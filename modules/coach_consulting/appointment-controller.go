//go:generate mockery --all --inpackage --case snake

package coach_consulting

import (
	"context"
	"fmt"
	"gitlab.com/Nacute/fita-be-test/dto"
	"time"
)

type Controller struct {
	uc UsecaseInterface
}

type ControllerInterface interface {
	CreateAppointment(
		ctx context.Context,
		payload createAppointmentPayload,
	) (*dto.Response, error)
	RescheduleAppointment(
		ctx context.Context,
		payload rescheduleAppointmentPayload,
	) (*dto.Response, error)
	ApprovalAppointment(
		ctx context.Context,
		payload approvalAppointment,
	) (*dto.Response, error)
}

func (c Controller) ApprovalAppointment(
	ctx context.Context,
	payload approvalAppointment,
) (*dto.Response, error) {
	start := time.Now()
	result, err := c.uc.ApprovalAppointment(ctx, payload)
	if err != nil {
		return &dto.Response{}, err
	}

	return dto.NewSuccessResponse(
		result,
		"approval success",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil
}

func (c Controller) RescheduleAppointment(
	ctx context.Context,
	payload rescheduleAppointmentPayload,
) (*dto.Response, error) {
	start := time.Now()
	result, err := c.uc.RescheduleAppointment(ctx, payload)
	if err != nil {
		return &dto.Response{}, err
	}

	return dto.NewSuccessResponse(
		result,
		"reschedule success",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil

}

func (c Controller) CreateAppointment(
	ctx context.Context,
	payload createAppointmentPayload,
) (*dto.Response, error) {
	start := time.Now()
	result, err := c.uc.CreateAppointment(ctx, payload)
	if err != nil {
		return &dto.Response{}, err
	}
	return dto.NewSuccessResponse(
		result,
		"Appointmant has been created",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil
}
