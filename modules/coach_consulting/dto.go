package coach_consulting

import (
	"gitlab.com/Nacute/fita-be-test/entities"
	"time"
)

type approvalAppointment struct {
	Reason        string `json:"reason"`
	Status        string `json:"status"`
	AppointmentID uint   `json:"-"`
}

type createAppointmentPayload struct {
	StartedAt  time.Time `json:"startedAt"`
	FinishedAt time.Time `json:"finishedAt"`
	CoachId    uint      `json:"coachId"`
	CustomerId uint      `json:"customerId"`
}

type rescheduleAppointmentPayload struct {
	StartedAt     time.Time `json:"startedAt"`
	FinishedAt    time.Time `json:"finishedAt"`
	AppointmentID uint      `json:"-"`
}

type ucValidationPayload struct {
	StartedAt     time.Time
	FinishedAt    time.Time
	CoachId       uint
	CustomerId    uint
	AppointmentID uint
}

type validationToSkip struct {
	CoachExit          bool
	CustExit           bool
	GetSchedule        bool
	GetAppointments    bool
	GetAppointmentByID bool
}

type validateResultData struct {
	schedules    []entities.Schedules
	appointments []entities.Appointments
	appointment  entities.Appointments
}
