//go:generate mockery --all --inpackage --case snake

package coach_consulting

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/Nacute/fita-be-test/constant"
	"gitlab.com/Nacute/fita-be-test/entities"
	"gitlab.com/Nacute/fita-be-test/repository"
	time2 "gitlab.com/Nacute/fita-be-test/utils/time"
	"golang.org/x/exp/slices"
	"strings"
	"sync"
	"time"
)

type Usecase struct {
	userRepo        repository.UserRepoInterface
	appointmentRepo repository.AppointmentRepoInterface
	scheduleRepo    repository.ScheduleRepoInterface
	clock           time2.Clock
}

type UsecaseInterface interface {
	CreateAppointment(
		ctx context.Context,
		payload createAppointmentPayload,
	) (*entities.Appointments, error)
	RescheduleAppointment(
		ctx context.Context,
		payload rescheduleAppointmentPayload,
	) (entities.Appointments, error)
	ApprovalAppointment(
		ctx context.Context,
		payload approvalAppointment,
	) (entities.Appointments, error)
}

func (uc Usecase) ApprovalAppointment(
	ctx context.Context,
	payload approvalAppointment,
) (entities.Appointments, error) {
	appointment, err := uc.appointmentRepo.GetByID(ctx, payload.AppointmentID)
	if err != nil {
		return entities.Appointments{}, err
	}

	if !(appointment.LatestStatus == constant.AppointmentCoachApproval ||
		appointment.LatestStatus == constant.AppointmentRescheduleApproval) {
		return entities.Appointments{}, constant.ErrAppointmentApproval
	}

	switch payload.Status {
	case constant.Approved:
		appointment.LatestStatus = constant.AppointmentBooked
	case constant.Rejected:
		appointment.LatestStatus = constant.AppointmentCanceled

	}
	appointment.UpdatedAt = uc.clock.Now()
	appointment, err = uc.appointmentRepo.Update(
		ctx,
		appointment,
		"LatestStatus",
		"UpdatedAt",
	)
	if err != nil {
		return entities.Appointments{}, err
	}

	return appointment, nil
}

func (uc Usecase) RescheduleAppointment(
	ctx context.Context,
	payload rescheduleAppointmentPayload,
) (entities.Appointments, error) {
	datas, err := uc.validateUserAndGetSchedule(
		ctx,
		ucValidationPayload{
			AppointmentID: payload.AppointmentID,
			StartedAt:     payload.StartedAt,
			FinishedAt:    payload.FinishedAt,
		}, &validationToSkip{
			CustExit:  true,
			CoachExit: true,
		})
	if err != nil {
		return entities.Appointments{}, err
	}

	eligableStatus := []string{
		constant.AppointmentCoachApproval,
		constant.AppointmentRescheduleApproval,
	}

	if !slices.Contains(eligableStatus, datas.appointment.LatestStatus) {
		return entities.Appointments{}, constant.ErrAppointmentReschedule
	}

	datas.appointment.StartedDate = payload.StartedAt
	datas.appointment.FinishedDate = payload.FinishedAt
	datas.appointment.LatestStatus = constant.AppointmentRescheduleApproval
	datas.appointment.UpdatedAt = uc.clock.Now()
	err = uc.evaluateSchedule(datas.schedules, datas.appointments, datas.appointment)
	if err != nil {
		return entities.Appointments{}, err
	}

	datas.appointment, err = uc.appointmentRepo.Update(
		ctx,
		datas.appointment,
		"LatestStatus",
		"StartedDate",
		"FinishedDate",
		"UpdatedAt",
	)
	if err != nil {
		return entities.Appointments{}, err
	}

	return datas.appointment, nil
}

func (uc Usecase) CreateAppointment(
	ctx context.Context,
	payload createAppointmentPayload,
) (*entities.Appointments, error) {
	datas, err := uc.validateUserAndGetSchedule(
		ctx,
		ucValidationPayload{
			StartedAt:  payload.StartedAt,
			FinishedAt: payload.FinishedAt,
			CoachId:    payload.CoachId,
			CustomerId: payload.CustomerId,
		}, &validationToSkip{
			GetAppointmentByID: true,
		},
	)
	if err != nil {
		return &entities.Appointments{}, err
	}

	var appointment = &entities.Appointments{
		StartedDate:  payload.StartedAt,
		FinishedDate: payload.FinishedAt,
	}
	err = uc.evaluateSchedule(datas.schedules, datas.appointments, *appointment)
	if err != nil {
		if errors.Is(constant.ErrCoachNotAvailable, err) {
			return &entities.Appointments{}, err
		}
		return &entities.Appointments{}, fmt.Errorf("evaluateSchedule: %w", err)
	}

	appointment.LatestStatus = constant.AppointmentCoachApproval
	appointment.CoachID = payload.CoachId
	appointment.CustomerID = payload.CustomerId
	appointment, err = uc.appointmentRepo.Store(ctx, appointment)

	return appointment, nil
}

type errWLabel struct {
	label string
	err   error
}

func (uc Usecase) validateUserAndGetSchedule(
	ctx context.Context,
	payload ucValidationPayload,
	skip *validationToSkip,
) (validateResultData, error) {
	var (
		wg         = new(sync.WaitGroup)
		errChan    = make(chan errWLabel)
		errWlabel  errWLabel
		err        error
		errCount   int
		custExist  bool
		coachExist bool
		result     validateResultData
	)

	if skip == nil {
		skip = &validationToSkip{}
	}

	if skip.GetAppointmentByID == false {
		result.appointment, err = uc.appointmentRepo.GetByID(ctx, payload.AppointmentID)
		if err != nil {
			return validateResultData{}, err
		}
		payload.CoachId = result.appointment.CoachID
	}

	if skip.CustExit == false {
		errCount++
		wg.Add(1)
		go func(ctx context.Context) {
			defer wg.Done()
			custExist, err = uc.userRepo.IsUserExist(ctx, payload.CustomerId)
			errChan <- errWLabel{
				label: "userRepo.IsUserExist(customer)",
				err:   err,
			}
		}(ctx)
	}
	if skip.CoachExit == false {
		errCount++
		wg.Add(1)
		go func(ctx context.Context) {
			defer wg.Done()
			coachExist, err = uc.userRepo.IsUserExist(ctx, payload.CoachId)
			errChan <- errWLabel{
				label: "userRepo.IsUserExist(coach)",
				err:   err,
			}
		}(ctx)
	}

	if skip.GetSchedule == false {
		errCount++
		wg.Add(1)
		go func(ctx context.Context) {
			defer wg.Done()
			result.schedules, err = uc.scheduleRepo.GetByCoachID(ctx, payload.CoachId)
			errChan <- errWLabel{
				label: "scheduleRepo.GetByCoachID",
				err:   err,
			}
		}(ctx)
	}

	if skip.GetAppointments == false {
		errCount++
		wg.Add(1)
		go func(ctx context.Context) {
			defer wg.Done()
			result.appointments, err = uc.appointmentRepo.GetByCoachID(ctx, payload.CoachId)
			errChan <- errWLabel{
				label: "scheduleRepo.GetByCoachID",
				err:   err,
			}
		}(ctx)
	}

	for i := 0; i < errCount; i++ {
		errWlabel = <-errChan
		if errWlabel.err != nil {
			return validateResultData{}, fmt.Errorf("%s: %w", errWlabel.label, errWlabel.err)
		}

	}

	wg.Wait()

	if !coachExist && skip.CoachExit == false {
		return validateResultData{}, constant.ErrCoachNotFound
	}

	if !custExist && skip.CustExit == false {
		return validateResultData{}, constant.ErrCustomerNotFound
	}

	return result, nil
}

func (uc Usecase) evaluateSchedule(
	schedules []entities.Schedules,
	appointments []entities.Appointments,
	payload entities.Appointments,
) error {

	day := payload.StartedDate.Weekday()
	// convert to date 2006-01-0* so that in same day with schedules
	startTz := uc.changeToBeSameDayAsSchedule(payload.StartedDate, int(day))
	endTz := uc.changeToBeSameDayAsSchedule(payload.FinishedDate, int(day))
	clientTz := payload.StartedDate.Location()

	eligableStatus := []string{
		constant.AppointmentBooked,
		constant.AppointmentCoachApproval,
		constant.AppointmentRescheduleApproval,
	}
	// check upon another appointment
	for i, _ := range appointments {
		if slices.Contains(eligableStatus, appointments[i].LatestStatus) {
			dayApp := appointments[i].StartedDate.Weekday()
			if strings.EqualFold(schedules[i].DayOfWeek, day.String()) {
				startApp := uc.changeToBeSameDayAsSchedule(appointments[i].StartedDate, int(dayApp))
				endTApp := uc.changeToBeSameDayAsSchedule(appointments[i].FinishedDate, int(dayApp))
				startApp = startApp.In(clientTz)
				endTApp = startApp.In(clientTz)
				if startApp.Before(startTz) &&
					endTApp.After(startTz) &&
					startApp.Before(endTz) &&
					endTApp.After(endTz) {
					return constant.ErrCoachNotAvailable
				}
			}
		}
	}

	// check upon schedule
	for i, _ := range schedules {
		startSch, err := schedules[i].ConvertToTime(true, clientTz)
		if err != nil {
			return err
		}
		endSch, err := schedules[i].ConvertToTime(false, clientTz)
		if err != nil {
			return err
		}
		if strings.EqualFold(schedules[i].DayOfWeek, day.String()) {
			if startSch.Before(startTz.Add(time.Second*1)) &&
				endSch.After(startTz.Add(time.Second*1)) &&
				startSch.Before(endTz.Add(time.Second*-1)) &&
				endSch.After(endTz.Add(time.Second*-1)) {
				return nil
			}
		}
	}

	return constant.ErrCoachNotAvailable
}

func (uc Usecase) changeToBeSameDayAsSchedule(t time.Time, day int) time.Time {
	return time.Date(
		2006,
		01,
		day+1,
		t.Hour(),
		t.Minute(),
		t.Second(),
		0,
		t.Location(),
	)
}
