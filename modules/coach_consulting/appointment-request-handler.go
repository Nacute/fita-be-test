//go:generate mockery --all --inpackage --case snake

package coach_consulting

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Nacute/fita-be-test/dto"
	"gitlab.com/Nacute/fita-be-test/middleware"
	"net/http"
	"strconv"
)

type Requesthandler struct {
	ctrl   ControllerInterface
	enigma middleware.EnigmaUtility
}

func (r Requesthandler) CreateAppointment(c *gin.Context) {
	var payload createAppointmentPayload
	if errs := r.enigma.BindAndValidate(c, &payload); len(errs) > 0 {
		c.JSON(http.StatusBadRequest, dto.DefaultInvalidInputResponse(errs))
		return
	}

	res, err := r.ctrl.CreateAppointment(c.Request.Context(), payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorResponse(err.Error(), ""))
		return
	}

	c.JSON(http.StatusOK, res)

}

func (r Requesthandler) RescheduleAppointment(c *gin.Context) {
	var payload rescheduleAppointmentPayload
	if errs := r.enigma.BindAndValidate(c, &payload); len(errs) > 0 {
		c.JSON(http.StatusBadRequest, dto.DefaultInvalidInputResponse(errs))
		return
	}
	id, err := strconv.ParseUint(c.Param("appointment_id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.DefaultErrorResponse(err.Error(), ""))
		return
	}
	payload.AppointmentID = uint(id)

	res, err := r.ctrl.RescheduleAppointment(c.Request.Context(), payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorResponse(err.Error(), ""))
		return
	}

	c.JSON(http.StatusOK, res)

}

func (r Requesthandler) ApprovalAppointment(c *gin.Context) {
	var payload approvalAppointment
	if errs := r.enigma.BindAndValidate(c, &payload); len(errs) > 0 {
		c.JSON(http.StatusBadRequest, dto.DefaultInvalidInputResponse(errs))
		return
	}
	id, err := strconv.ParseUint(c.Param("appointment_id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.DefaultErrorResponse(err.Error(), ""))
		return
	}
	payload.AppointmentID = uint(id)

	res, err := r.ctrl.ApprovalAppointment(c.Request.Context(), payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorResponse(err.Error(), ""))
		return
	}

	c.JSON(http.StatusOK, res)
}
