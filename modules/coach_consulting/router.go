package coach_consulting

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Nacute/fita-be-test/middleware"
	"gitlab.com/Nacute/fita-be-test/repository"
	"gitlab.com/Nacute/fita-be-test/utils/time"
	"gorm.io/gorm"
)

type Router struct {
	rh *Requesthandler
}

func NewRouter(db *gorm.DB, engima middleware.EnigmaUtility, clock time.Clock) Router {
	return Router{
		rh: &Requesthandler{
			ctrl: &Controller{
				uc: &Usecase{
					userRepo:        repository.NewUserRepo(db),
					appointmentRepo: repository.NewAppointmentRepo(db),
					scheduleRepo:    repository.NewScheduleRepo(db),
					clock:           clock,
				},
			},
			enigma: engima,
		},
	}
}

func (r Router) Route(handler *gin.RouterGroup) {
	appointment := handler.Group("/appointment")
	appointment.POST("/", r.rh.CreateAppointment)
	appointment.PUT("/:appointment_id/approval", r.rh.ApprovalAppointment)
	appointment.PUT("/:appointment_id/reschedule", r.rh.RescheduleAppointment)

}
