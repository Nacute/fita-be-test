//go:generate mockery --all --inpackage --case snake

package coach_consulting

import (
	"context"
	"fmt"
	"gitlab.com/Nacute/fita-be-test/dto"
	"gitlab.com/Nacute/fita-be-test/entities"
	"gitlab.com/Nacute/fita-be-test/utils/test_utils"
	"reflect"
	"testing"
)

type ctrlMockList struct {
	uc *MockUsecaseInterface
}

const (
	ctrlCreateAppointment = iota
	ctrlCreateAppointmentErr
	ctrlRescheduleAppointment
	ctrlRescheduleAppointmentErr
	ctrlApprovalAppointment
	ctrlApprovalAppointmentErr
)

func defaultCtrlMockCalls(mcks *ctrlMockList) map[int]test_utils.MockCall {
	return map[int]test_utils.MockCall{
		ctrlCreateAppointment: {
			Mock:   &mcks.uc.Mock,
			Method: "CreateAppointment",
			Args: []any{
				context.Background(),
				createAppointmentPayload{},
			},
			Result: []any{
				&entities.Appointments{},
				nil,
			},
			Times: 1,
		},
		ctrlCreateAppointmentErr: {
			Mock:   &mcks.uc.Mock,
			Method: "CreateAppointment",
			Args: []any{
				context.Background(),
				createAppointmentPayload{},
			},
			Result: []any{
				nil,
				fmt.Errorf("error"),
			},
			Times: 1,
		},
		ctrlRescheduleAppointment: {
			Mock:   &mcks.uc.Mock,
			Method: "RescheduleAppointment",
			Args:   []any{context.Background(), rescheduleAppointmentPayload{}},
			Result: []any{entities.Appointments{}, nil},
			Times:  1,
		},
		ctrlRescheduleAppointmentErr: {
			Mock:   &mcks.uc.Mock,
			Method: "RescheduleAppointment",
			Args:   []any{context.Background(), rescheduleAppointmentPayload{}},
			Result: []any{entities.Appointments{}, fmt.Errorf("error")},
			Times:  1,
		},
		ctrlApprovalAppointment: {
			Mock:   &mcks.uc.Mock,
			Method: "ApprovalAppointment",
			Args:   []any{context.Background(), approvalAppointment{}},
			Result: []any{entities.Appointments{}, nil},
			Times:  1,
		},
		ctrlApprovalAppointmentErr: {
			Mock:   &mcks.uc.Mock,
			Method: "ApprovalAppointment",
			Args:   []any{context.Background(), approvalAppointment{}},
			Result: []any{entities.Appointments{}, fmt.Errorf("error")},
			Times:  1,
		},
	}
}

func TestController_CreateAppointment(t *testing.T) {
	type args struct {
		ctx     context.Context
		payload createAppointmentPayload
	}
	tests := []struct {
		name    string
		fields  func(mcks *ctrlMockList)
		args    args
		want    *dto.Response
		wantErr bool
	}{
		{
			name: "success",
			fields: func(mcks *ctrlMockList) {
				mockCalls := defaultCtrlMockCalls(mcks)
				test_utils.GenerateMockMethod([]test_utils.MockCall{
					mockCalls[ctrlCreateAppointment],
				})
			},
			args: args{
				ctx:     context.Background(),
				payload: createAppointmentPayload{},
			},
			want: dto.NewSuccessResponse(
				&entities.Appointments{},
				"Appointmant has been created",
				"0.ms",
			),
			wantErr: false,
		},
		{
			name: "error",
			fields: func(mcks *ctrlMockList) {
				mockCalls := defaultCtrlMockCalls(mcks)
				test_utils.GenerateMockMethod([]test_utils.MockCall{
					mockCalls[ctrlCreateAppointmentErr],
				})
			},
			args: args{
				ctx:     context.Background(),
				payload: createAppointmentPayload{},
			},
			want:    &dto.Response{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMocks := &ctrlMockList{
				uc: NewMockUsecaseInterface(t),
			}

			if tt.fields != nil {
				tt.fields(listMocks)
			}

			c := Controller{
				uc: listMocks.uc,
			}
			got, err := c.CreateAppointment(tt.args.ctx, tt.args.payload)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateAppointment() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateAppointment() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestController_RescheduleAppointment(t *testing.T) {
	type args struct {
		ctx     context.Context
		payload rescheduleAppointmentPayload
	}
	tests := []struct {
		name    string
		fields  func(mcks *ctrlMockList)
		args    args
		want    *dto.Response
		wantErr bool
	}{
		{
			name: "success",
			fields: func(mcks *ctrlMockList) {
				mockCalls := defaultCtrlMockCalls(mcks)
				test_utils.GenerateMockMethod([]test_utils.MockCall{
					mockCalls[ctrlRescheduleAppointment],
				})
			},
			args: args{
				ctx:     context.Background(),
				payload: rescheduleAppointmentPayload{},
			},
			want: dto.NewSuccessResponse(
				entities.Appointments{},
				"reschedule success",
				"0.ms",
			),
			wantErr: false,
		},
		{
			name: "error",
			fields: func(mcks *ctrlMockList) {
				mockCalls := defaultCtrlMockCalls(mcks)
				test_utils.GenerateMockMethod([]test_utils.MockCall{
					mockCalls[ctrlRescheduleAppointmentErr],
				})
			},
			args: args{
				ctx:     context.Background(),
				payload: rescheduleAppointmentPayload{},
			},
			want:    &dto.Response{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMocks := &ctrlMockList{
				uc: NewMockUsecaseInterface(t),
			}

			if tt.fields != nil {
				tt.fields(listMocks)
			}

			c := Controller{
				uc: listMocks.uc,
			}
			got, err := c.RescheduleAppointment(tt.args.ctx, tt.args.payload)
			if (err != nil) != tt.wantErr {
				t.Errorf("RescheduleAppointment() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RescheduleAppointment() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestController_ApprovalAppointment(t *testing.T) {
	type args struct {
		ctx     context.Context
		payload approvalAppointment
	}
	tests := []struct {
		name    string
		fields  func(mcks *ctrlMockList)
		args    args
		want    *dto.Response
		wantErr bool
	}{
		{
			name: "success",
			fields: func(mcks *ctrlMockList) {
				mockCalls := defaultCtrlMockCalls(mcks)
				test_utils.GenerateMockMethod([]test_utils.MockCall{
					mockCalls[ctrlApprovalAppointment],
				})
			},
			args: args{
				ctx:     context.Background(),
				payload: approvalAppointment{},
			},
			want: dto.NewSuccessResponse(
				entities.Appointments{},
				"approval success",
				"0.ms",
			),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMocks := &ctrlMockList{
				uc: NewMockUsecaseInterface(t),
			}

			if tt.fields != nil {
				tt.fields(listMocks)
			}

			c := Controller{
				uc: listMocks.uc,
			}
			got, err := c.ApprovalAppointment(tt.args.ctx, tt.args.payload)
			if (err != nil) != tt.wantErr {
				t.Errorf("ApprovalAppointment() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ApprovalAppointment() got = %v, want %v", got, tt.want)
			}
		})
	}
}
