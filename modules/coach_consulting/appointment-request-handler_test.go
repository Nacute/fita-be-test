package coach_consulting

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Nacute/fita-be-test/dto"
	"gitlab.com/Nacute/fita-be-test/middleware"
	"gitlab.com/Nacute/fita-be-test/utils/test_utils"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

type newRequestParam struct {
	method string
	url    string
	body   any
}

type rqMockList struct {
	ctrl   *MockControllerInterface
	enigma *middleware.MockEnigmaUtility
}

const (
	rqBindAndValidate = iota
	rqApprovalAppointment
)

func defaultRqMockCall(mcks *rqMockList) map[int]test_utils.MockCall {
	return map[int]test_utils.MockCall{
		rqBindAndValidate: {
			Mock:   &mcks.enigma.Mock,
			Method: "BindAndValidate",
			Result: []any{nil},
			Times:  1,
		},
		rqApprovalAppointment: {
			Mock:   &mcks.ctrl.Mock,
			Method: "ApprovalAppointment",
			Args:   []any{context.Background(), approvalAppointment{AppointmentID: uint(1)}},
			Result: []any{&dto.Response{
				ResponseMeta: dto.ResponseMeta{
					Success:      true,
					MessageTitle: "Success",
				},
			}, nil},
			Times: 1,
		},
	}
}

func TestRequesthandler_ApprovalAppointment(t *testing.T) {
	type args struct {
		c *gin.Context
	}

	gin.SetMode(gin.TestMode)
	tests := []struct {
		name            string
		mocks           func(list *rqMockList, c *gin.Context)
		arg             args
		newRequestParam newRequestParam
		req             func(params newRequestParam) *http.Request
		reqParams       gin.Params
		code            int
		resExpect       dto.Response
		resActual       *httptest.ResponseRecorder
	}{
		{
			name: "success",
			mocks: func(list *rqMockList, c *gin.Context) {
				mocksList := defaultRqMockCall(list)
				bindVal := mocksList[rqBindAndValidate]
				var payload approvalAppointment
				bindVal.Args = []any{c, &payload}
				test_utils.GenerateMockMethod([]test_utils.MockCall{
					bindVal,
					mocksList[rqApprovalAppointment],
				})
			},
			newRequestParam: newRequestParam{
				method: http.MethodPut,
				url:    "/1/approval",
				body:   approvalAppointment{},
			},
			req: func(params newRequestParam) *http.Request {
				var body []byte
				body, err := json.Marshal(params.body)
				if err != nil {
					log.Println(fmt.Errorf("json.Marshal: %w", err))
					t.Fail()
					return &http.Request{}
				}
				req, err := http.NewRequest(params.method, params.url, bytes.NewReader(body))
				if err != nil {
					log.Println(fmt.Errorf("new request: %w", err))
					t.Fail()
					return &http.Request{}
				}
				return req
			},
			reqParams: gin.Params{
				gin.Param{
					Key:   "appointment_id",
					Value: "1",
				},
			},
			code: 200,
			resExpect: dto.Response{
				ResponseMeta: dto.ResponseMeta{
					Success:      true,
					MessageTitle: "Success",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMocks := &rqMockList{
				ctrl:   NewMockControllerInterface(t),
				enigma: middleware.NewMockEnigmaUtility(t),
			}

			res := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(res)
			c.Request = tt.req(tt.newRequestParam)
			c.Params = tt.reqParams
			tt.arg.c = c
			if tt.mocks != nil {
				tt.mocks(listMocks, c)
			}
			rh := Requesthandler{
				ctrl:   listMocks.ctrl,
				enigma: listMocks.enigma,
			}

			rh.ApprovalAppointment(tt.arg.c)

			var resMeta dto.Response
			body := res.Body.Bytes()
			err := json.Unmarshal(body, &resMeta)
			if err != nil {
				log.Println(fmt.Errorf("json.Unmarshal: %w", err))
				t.Fail()
				return
			}

			assert.Equal(t, tt.resExpect, resMeta)
			assert.Equal(t, tt.code, res.Code)
		})
	}
}
