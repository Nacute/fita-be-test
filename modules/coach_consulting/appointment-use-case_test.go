package coach_consulting

import (
	"context"
	"gitlab.com/Nacute/fita-be-test/constant"
	"gitlab.com/Nacute/fita-be-test/entities"
	"gitlab.com/Nacute/fita-be-test/repository"
	"gitlab.com/Nacute/fita-be-test/utils/test_utils"
	time2 "gitlab.com/Nacute/fita-be-test/utils/time"
	"reflect"
	"testing"
	"time"
)

type ucMockList struct {
	userRepo        *repository.MockUserRepoInterface
	appointmentRepo *repository.MockAppointmentRepoInterface
	scheduleRepo    *repository.MockScheduleRepoInterface
	clock           *time2.MockClock
}

const (
	ucIsCoachExist = iota
	ucIsCustExist
	ucGetByCoachID
	ucAppointGetByCoachID
	ucAppointGetByID
	ucAppointmentStore
	ucAppointmentUpdate
	ucAppointmentUpdateStatus
	ucClockNow
)

func defaultUcMockCalls(mck *ucMockList) map[int]test_utils.MockCall {
	singaporeZone, _ := time.LoadLocation("Asia/Singapore")
	return map[int]test_utils.MockCall{
		ucAppointGetByID: {
			Mock:   &mck.appointmentRepo.Mock,
			Method: "GetByID",
			Args: []any{
				context.Background(),
				uint(1),
			},
			Result: []any{
				entities.Appointments{
					ID:           1,
					CoachID:      uint(2),
					LatestStatus: constant.AppointmentCoachApproval,
				},
				nil,
			},
			Times: 1,
		},
		ucIsCustExist: {
			Mock:   &mck.userRepo.Mock,
			Method: "IsUserExist",
			Args: []any{
				context.Background(),
				uint(1),
			},
			Result: []any{
				true, nil,
			},
			Times: 1,
		},
		ucIsCoachExist: {
			Mock:   &mck.userRepo.Mock,
			Method: "IsUserExist",
			Args: []any{
				context.Background(),
				uint(2),
			},
			Result: []any{
				true, nil,
			},
			Times: 1,
		},
		ucGetByCoachID: {
			Mock:   &mck.scheduleRepo.Mock,
			Method: "GetByCoachID",
			Args: []any{
				context.Background(),
				uint(2),
			},
			Result: []any{
				[]entities.Schedules{
					{
						TimezoneName: entities.TimezoneName{Name: "Asia/Jakarta"},
						DayOfWeek:    "Thursday",
						StartedAt:    "12:59:00",
						FinishedAt:   "14:00:00",
					},
					{
						TimezoneName: entities.TimezoneName{Name: "Asia/Jakarta"},
						DayOfWeek:    "Thursday",
						StartedAt:    "10:00:00",
						FinishedAt:   "12:00:00",
					},
				}, nil,
			},
			Times: 1,
		},
		ucAppointGetByCoachID: {
			Mock:   &mck.appointmentRepo.Mock,
			Method: "GetByCoachID",
			Args: []any{
				context.Background(),
				uint(2),
			},
			Result: []any{
				[]entities.Appointments{
					{
						CoachID:      uint(2),
						CustomerID:   uint(3),
						LatestStatus: constant.AppointmentBooked,
						StartedDate:  time.Date(2023, 3, 30, 13, 59, 0, 0, singaporeZone),
						FinishedDate: time.Date(2023, 3, 30, 15, 0, 0, 0, singaporeZone),
					},
				}, nil,
			},
			Times: 1,
		},
		ucAppointmentStore: {
			Mock:   &mck.appointmentRepo.Mock,
			Method: "Store",
			Args: []any{
				context.Background(),
				&entities.Appointments{
					CoachID:      uint(2),
					CustomerID:   uint(1),
					LatestStatus: constant.AppointmentCoachApproval,
					StartedDate:  time.Date(2023, 3, 30, 11, 00, 0, 0, singaporeZone),
					FinishedDate: time.Date(2023, 3, 30, 12, 0, 0, 0, singaporeZone)},
			},
			Result: []any{
				&entities.Appointments{
					CoachID:      uint(2),
					CustomerID:   uint(1),
					LatestStatus: constant.AppointmentCoachApproval,
					StartedDate:  time.Date(2023, 3, 30, 11, 00, 0, 0, singaporeZone),
					FinishedDate: time.Date(2023, 3, 30, 12, 0, 0, 0, singaporeZone)}, nil,
			},
			Times: 1,
		},
		ucAppointmentUpdate: {
			Mock:   &mck.appointmentRepo.Mock,
			Method: "Update",
			Args: []any{
				context.Background(),
				entities.Appointments{
					ID:           uint(1),
					CoachID:      uint(2),
					LatestStatus: constant.AppointmentRescheduleApproval,
					StartedDate:  time.Date(2023, 3, 30, 11, 00, 0, 0, singaporeZone),
					FinishedDate: time.Date(2023, 3, 30, 12, 0, 0, 0, singaporeZone),
				},
				"LatestStatus",
				"StartedDate",
				"FinishedDate",
				"UpdatedAt",
			},
			Result: []any{
				entities.Appointments{
					ID:           uint(1),
					CoachID:      uint(2),
					LatestStatus: constant.AppointmentRescheduleApproval,
					StartedDate:  time.Date(2023, 3, 30, 11, 00, 0, 0, singaporeZone),
					FinishedDate: time.Date(2023, 3, 30, 12, 0, 0, 0, singaporeZone)}, nil,
			},
			Times: 1,
		},
		ucAppointmentUpdateStatus: {
			Mock:   &mck.appointmentRepo.Mock,
			Method: "Update",
			Args: []any{
				context.Background(),
				entities.Appointments{
					ID:           uint(1),
					CoachID:      uint(2),
					LatestStatus: constant.AppointmentBooked,
				},
				"LatestStatus",
				"UpdatedAt",
			},
			Result: []any{
				entities.Appointments{
					ID:           uint(1),
					CoachID:      uint(2),
					LatestStatus: constant.AppointmentBooked,
				}, nil,
			},
			Times: 1,
		},

		ucClockNow: {
			Mock:   &mck.clock.Mock,
			Method: "Now",
			Args:   []any{},
			Result: []any{time.Time{}},
			Times:  1,
		},
	}
}

func TestUsecase_CreateAppointment(t *testing.T) {
	type args struct {
		ctx     context.Context
		payload createAppointmentPayload
	}
	singaporeZone, _ := time.LoadLocation("Asia/Singapore")
	tests := []struct {
		name    string
		fields  func(mcks *ucMockList)
		args    args
		want    *entities.Appointments
		wantErr bool
	}{
		{
			name: "success",
			fields: func(mcks *ucMockList) {
				mckList := defaultUcMockCalls(mcks)
				test_utils.GenerateMockMethod([]test_utils.MockCall{
					mckList[ucIsCoachExist],
					mckList[ucIsCustExist],
					mckList[ucGetByCoachID],
					mckList[ucAppointGetByCoachID],
					mckList[ucAppointmentStore],
				})
			},
			args: args{
				ctx: context.Background(),
				payload: createAppointmentPayload{
					StartedAt:  time.Date(2023, 3, 30, 11, 00, 0, 0, singaporeZone),
					FinishedAt: time.Date(2023, 3, 30, 12, 0, 0, 0, singaporeZone),
					CoachId:    uint(2),
					CustomerId: uint(1),
				},
			},
			want: &entities.Appointments{
				CoachID:      uint(2),
				CustomerID:   uint(1),
				LatestStatus: constant.AppointmentCoachApproval,
				StartedDate:  time.Date(2023, 3, 30, 11, 0, 0, 0, singaporeZone),
				FinishedDate: time.Date(2023, 3, 30, 12, 0, 0, 0, singaporeZone),
			},
			wantErr: false,
		},
		{
			name: "coach is not available",
			fields: func(mcks *ucMockList) {
				mckList := defaultUcMockCalls(mcks)
				test_utils.GenerateMockMethod([]test_utils.MockCall{
					mckList[ucIsCoachExist],
					mckList[ucIsCustExist],
					mckList[ucGetByCoachID],
					mckList[ucAppointGetByCoachID],
				})
			},
			args: args{
				ctx: context.Background(),
				payload: createAppointmentPayload{
					StartedAt:  time.Date(2023, 3, 30, 13, 59, 0, 0, singaporeZone),
					FinishedAt: time.Date(2023, 3, 30, 15, 59, 0, 0, singaporeZone),
					CoachId:    uint(2),
					CustomerId: uint(1),
				},
			},
			want:    &entities.Appointments{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMock := &ucMockList{
				userRepo:        repository.NewMockUserRepoInterface(t),
				appointmentRepo: repository.NewMockAppointmentRepoInterface(t),
				scheduleRepo:    repository.NewMockScheduleRepoInterface(t),
				clock:           time2.NewMockClock(t),
			}

			if tt.fields != nil {
				tt.fields(listMock)
			}
			uc := Usecase{
				userRepo:        listMock.userRepo,
				appointmentRepo: listMock.appointmentRepo,
				scheduleRepo:    listMock.scheduleRepo,
				clock:           listMock.clock,
			}
			got, err := uc.CreateAppointment(tt.args.ctx, tt.args.payload)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateAppointment() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateAppointment() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUsecase_RescheduleAppointment(t *testing.T) {
	type args struct {
		ctx     context.Context
		payload rescheduleAppointmentPayload
	}
	singaporeZone, _ := time.LoadLocation("Asia/Singapore")
	tests := []struct {
		name    string
		fields  func(mcks *ucMockList)
		args    args
		want    entities.Appointments
		wantErr bool
	}{
		{
			name: "success",
			fields: func(mcks *ucMockList) {
				mckList := defaultUcMockCalls(mcks)
				test_utils.GenerateMockMethod([]test_utils.MockCall{
					mckList[ucAppointGetByID],
					mckList[ucGetByCoachID],
					mckList[ucAppointGetByCoachID],
					mckList[ucClockNow],
					mckList[ucAppointmentUpdate],
				})
			},
			args: args{
				ctx: context.Background(),
				payload: rescheduleAppointmentPayload{
					AppointmentID: uint(1),
					StartedAt:     time.Date(2023, 3, 30, 11, 00, 0, 0, singaporeZone),
					FinishedAt:    time.Date(2023, 3, 30, 12, 0, 0, 0, singaporeZone),
				},
			},
			want: entities.Appointments{
				ID:           uint(1),
				CoachID:      uint(2),
				LatestStatus: constant.AppointmentRescheduleApproval,
				StartedDate:  time.Date(2023, 3, 30, 11, 00, 0, 0, singaporeZone),
				FinishedDate: time.Date(2023, 3, 30, 12, 0, 0, 0, singaporeZone),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMock := &ucMockList{
				userRepo:        repository.NewMockUserRepoInterface(t),
				appointmentRepo: repository.NewMockAppointmentRepoInterface(t),
				scheduleRepo:    repository.NewMockScheduleRepoInterface(t),
				clock:           time2.NewMockClock(t),
			}

			if tt.fields != nil {
				tt.fields(listMock)
			}
			uc := Usecase{
				userRepo:        listMock.userRepo,
				appointmentRepo: listMock.appointmentRepo,
				scheduleRepo:    listMock.scheduleRepo,
				clock:           listMock.clock,
			}
			got, err := uc.RescheduleAppointment(tt.args.ctx, tt.args.payload)
			if (err != nil) != tt.wantErr {
				t.Errorf("RescheduleAppointment() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RescheduleAppointment() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUsecase_ApprovalAppointment(t *testing.T) {
	type args struct {
		ctx     context.Context
		payload approvalAppointment
	}
	tests := []struct {
		name    string
		fields  func(mcks *ucMockList)
		args    args
		want    entities.Appointments
		wantErr bool
	}{
		{
			name: "success approved",
			fields: func(mcks *ucMockList) {
				mckList := defaultUcMockCalls(mcks)
				test_utils.GenerateMockMethod([]test_utils.MockCall{
					mckList[ucAppointGetByID],
					mckList[ucClockNow],
					mckList[ucAppointmentUpdateStatus],
				})
			},
			args: args{
				ctx: context.Background(),
				payload: approvalAppointment{
					Reason:        "",
					Status:        constant.Approved,
					AppointmentID: uint(1),
				},
			},
			want: entities.Appointments{
				ID:           uint(1),
				CoachID:      uint(2),
				LatestStatus: constant.AppointmentBooked,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMock := &ucMockList{
				userRepo:        repository.NewMockUserRepoInterface(t),
				appointmentRepo: repository.NewMockAppointmentRepoInterface(t),
				scheduleRepo:    repository.NewMockScheduleRepoInterface(t),
				clock:           time2.NewMockClock(t),
			}

			if tt.fields != nil {
				tt.fields(listMock)
			}
			uc := Usecase{
				userRepo:        listMock.userRepo,
				appointmentRepo: listMock.appointmentRepo,
				scheduleRepo:    listMock.scheduleRepo,
				clock:           listMock.clock,
			}
			got, err := uc.ApprovalAppointment(tt.args.ctx, tt.args.payload)
			if (err != nil) != tt.wantErr {
				t.Errorf("ApprovalAppointment() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ApprovalAppointment() got = %v, want %v", got, tt.want)
			}
		})
	}
}
