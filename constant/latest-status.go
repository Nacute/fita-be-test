package constant

const AppointmentCoachApproval = "appointment-coach-approval"
const AppointmentBooked = "appointment-booked"
const AppointmentRescheduleApproval = "appointment-reschedule-approval"
const AppointmentCanceled = "appointment-canceled"

const Approved = "disetujui"
const Rejected = "ditolak"
