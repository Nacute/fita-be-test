package constant

import "fmt"

var (
	ErrCoachNotAvailable     = fmt.Errorf("sorry, coach is not available at this schedule")
	ErrCoachNotFound         = fmt.Errorf("coach is not found")
	ErrCustomerNotFound      = fmt.Errorf("customer is not found")
	ErrAppointmentApproval   = fmt.Errorf("appointment status not eligable for approval")
	ErrAppointmentReschedule = fmt.Errorf("appointment status not eligable for reschedule")
)
