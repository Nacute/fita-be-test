//go:generate mockery --all --inpackage --case snake

package time

import "time"

type clock struct {
}

type Clock interface {
	Now() time.Time
	NowUnix() int64
}

func Default() Clock {
	return &clock{}
}

func (t clock) Now() time.Time {
	return time.Now()
}

func (t clock) NowUnix() int64 {
	return time.Now().Unix()
}
