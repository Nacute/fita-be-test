package test_utils

import (
	"database/sql/driver"
	"reflect"
	"testing"
)

type UserTest struct {
	ID     uint     `json:"id"`
	Name   string   `json:"name"`
	Status []Status `json:"status"`
	Code   string   `json:"code"`
}

type Status struct {
	Name string `json:"name"`
}

func TestStructToSliceOfArgs(t *testing.T) {
	type args[T interface{ any }] struct {
		data    T
		exclude []string
	}
	type testCase[T interface{ any }] struct {
		name string
		args args[T]
		want []driver.Value
	}
	tests := []testCase[UserTest]{
		{
			name: "success",
			args: args[UserTest]{
				exclude: []string{"Code"},
				data: UserTest{
					ID:   1,
					Name: "ridho",
					Status: []Status{
						{
							Name: "success",
						},
					},
				},
			},
			want: []driver.Value{
				uint(1), "ridho", []Status{{Name: "success"}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := StructToSliceOfArgs(tt.args.data, tt.args.exclude); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("StructToSliceOfArgs() = %v, want %v", got, tt.want)
			}
		})
	}
}
