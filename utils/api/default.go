package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/Nacute/fita-be-test/middleware"
	"gitlab.com/Nacute/fita-be-test/modules/coach_consulting"
	"gitlab.com/Nacute/fita-be-test/modules/health_check"
	"gitlab.com/Nacute/fita-be-test/utils/db"
	time2 "gitlab.com/Nacute/fita-be-test/utils/time"
)

func Default() *Api {
	server := gin.Default()

	dbConn, err := db.Default()
	if err != nil {
		panic(fmt.Sprintf("panic at db connection: %s", err.Error()))
	}
	enigma := middleware.NewEnigma()
	clock := time2.Default()
	var routers = []Router{
		coach_consulting.NewRouter(dbConn, enigma, clock),
		health_check.NewRouter(dbConn),
	}

	return &Api{
		engine:  server,
		routers: routers,
	}

}
