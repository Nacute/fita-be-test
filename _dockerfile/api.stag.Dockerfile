FROM golang:1.19-alpine3.17 as builder

# add some necessary packages
RUN apk update && \
    apk add libc-dev && \
    apk add gcc && \
    apk add make && \
    mkdir /src

WORKDIR /src
COPY ./../ ./
RUN go mod vendor && go mod verify
RUN go build -o /app-bin /src/app/main.go

# Distribution
FROM alpine:3
RUN apk --update add tzdata

WORKDIR /srv
COPY . .
COPY --from=builder /app-bin .

EXPOSE 8080
CMD ["/srv/app-bin"]