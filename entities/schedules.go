package entities

import (
	"fmt"
	"gitlab.com/Nacute/fita-be-test/constant"
	"time"
)

type Schedules struct {
	ID             uint         `gorm:"type:bigint auto_increment;primaryKey" json:"id"`
	CoachID        uint         `gorm:"column:coach_id;type:bigint unsigned not null" json:"coachId"`
	TimezoneNameID string       `gorm:"column:time_zone_name_Name;type:char(64);default;'Asia/Singapore'" json:"timeZoneNameId"`
	TimezoneName   TimezoneName `gorm:"foreignKey:TimezoneNameID" json:"timezoneName"`
	DayOfWeek      string       `gorm:"column:day_of_week;type:varchar(50) not null" json:"dayOfWeek"`
	StartedAt      string       `gorm:"column:started_at;type:time not null;default:'00:00:00'" json:"startedAt"`
	FinishedAt     string       `gorm:"column:finished_at;type:time not null;default:'00:00:00'" json:"finishedAt"`
	CreatedAt      time.Time    `gorm:"column:created_at;type:timestamp;default:CURRENT_TIMESTAMP" json:"createdAt"`
	UpdatedAt      time.Time    `gorm:"column:updated_at;type:timestamp;default:CURRENT_TIMESTAMP" json:"updatedAt"`
}

func (s Schedules) TableName() string {
	return "schedules"
}

func (s Schedules) ConvertToTime(started bool, tz *time.Location) (time.Time, error) {
	t := s.FinishedAt
	if started {
		t = s.StartedAt
	}
	dbLoc, err := time.LoadLocation(s.TimezoneName.Name)
	if err != nil {
		return time.Time{}, err
	}
	days := constant.WeekDayToDate
	//ds, _ := time.Date(2006/, 1, days[s.DayOfWeek], )
	timeParsed, err := time.ParseInLocation(
		"2006-01-02T15:04:05",
		fmt.Sprintf(
			"2006-01-0%dT%s",
			days[s.DayOfWeek],
			t,
		), dbLoc,
	)
	if err != nil {
		return time.Time{}, err
	}

	return timeParsed.In(tz), nil
}
