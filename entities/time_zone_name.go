package entities

type TimezoneName struct {
	Name       string `gorm:"column:Name;type:char(64) not null;primaryKey" json:"name"`
	TimeZoneID int    `gorm:"column:Time_zone_id;type:int unsigned not null"`
}

func (n TimezoneName) TableName() string {
	return "time_zone_name"
}
