package entities

import "time"

type MainUser struct {
	ID        uint      `gorm:"type:bigint unsigned auto_increment;primaryKey" json:"id"`
	Name      string    `gorm:"type:varchar(200) not null" json:"name"`
	RoleID    int       `gorm:"type:int not null;column:role_id" json:"roleId"`
	CreatedAt time.Time `gorm:"type:timestamp null;column:created_at;default:current_timestamp" json:"createdAt"`
	UpdatedAt time.Time `gorm:"type:timestamp null;column:updated_at;default:current_timestamp" json:"updatedAt"`
}

func (u MainUser) TableName() string {
	return "main_users"
}
