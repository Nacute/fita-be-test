package entities

import "time"

type Appointments struct {
	ID           uint      `gorm:"type:bigint auto_increment;primaryKey" json:"id"`
	CoachID      uint      `gorm:"column:coach_id;type:bigint unsigned not null" json:"coachId"`
	CustomerID   uint      `gorm:"column:customer_id;type:bigint unsigned not null" json:"customerId"`
	LatestStatus string    `gorm:"column:latest_status;type:varchar(100)" json:"latestStatus"`
	StartedDate  time.Time `gorm:"column:started_date;type:timestamp not null" json:"startedDate"`
	FinishedDate time.Time `gorm:"column:finished_date;type:timestamp not null" json:"finishedDate"`
	CreatedAt    time.Time `gorm:"column:created_at;type:timestamp;default:CURRENT_TIMESTAMP" json:"createdAt"`
	UpdatedAt    time.Time `gorm:"column:updated_at;type:timestamp;default:CURRENT_TIMESTAMP" json:"updatedAt"`
}

func (a Appointments) TableName() string {
	return "appoinments"
}
