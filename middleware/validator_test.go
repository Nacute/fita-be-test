package middleware

import (
	"github.com/gin-gonic/gin"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	"gitlab.com/Nacute/fita-be-test/utils/test_utils"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestEnigma_BindAndValidate(t *testing.T) {
	type fields struct {
		engine *validator.Validate
		trans  ut.Translator
	}
	type args struct {
		c       *gin.Context
		payload any
	}
	gin.SetMode(gin.TestMode)

	tests := []struct {
		name   string
		fields fields
		req    *http.Request
		args   args
		want   map[string][]string
	}{
		{
			name: "success",
			args: args{
				payload: TestStruct{
					Nama: "Ridho",
				},
			},
			req: test_utils.GenerateMockRequest(
				t,
				TestStruct{Nama: "Ridho"},
				http.MethodPost,
				"/test"),
			want: map[string][]string{},
		},
		{
			name: "error required",
			req: test_utils.GenerateMockRequest(
				t,
				TestStruct{},
				http.MethodPost,
				"/test"),
			want: map[string][]string{
				"nama": {"nama is a required field"},
			},
			args: args{
				payload: TestStruct{},
			},
		},
		{
			name: "error bind",
			req: test_utils.GenerateMockRequest(
				t,
				"404 not found",
				http.MethodPost,
				"/test"),
			want: map[string][]string{
				"nama": {"nama is a required field"},
			},
			args: args{
				payload: TestStruct{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			eng := NewEnigma().(*Enigma)
			tt.fields.engine = eng.engine
			tt.fields.trans = eng.trans
			c, _ := gin.CreateTestContext(httptest.NewRecorder())
			c.Request = tt.req
			tt.args.c = c
			v := Enigma{
				engine: tt.fields.engine,
				trans:  tt.fields.trans,
			}

			if got := v.BindAndValidate(tt.args.c, tt.args.payload); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BindAndValidate() = %v, want %v", got, tt.want)
			}
		})
	}
}
