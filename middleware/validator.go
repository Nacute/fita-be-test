//go:generate mockery --all --inpackage --case snake

package middleware

import (
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	en2 "github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	translations_en "github.com/go-playground/validator/v10/translations/en"
	"log"
	"reflect"
	"strings"

	"github.com/go-playground/validator/v10"
)

type Enigma struct {
	engine *validator.Validate
	trans  ut.Translator
}

func NewEnigma() EnigmaUtility {
	engine := validator.New()
	engine.RegisterTagNameFunc(func(field reflect.StructField) string {
		name := strings.SplitN(field.Tag.Get("json"), ",", 2)[0]

		if name == "-" {
			return ""
		}

		return name
	})

	en := en2.New()
	uni := ut.New(en, en)
	trans, _ := uni.GetTranslator("en")
	err := translations_en.RegisterDefaultTranslations(engine, trans)
	if err != nil {
		log.Println("new validator: ", err)
		return nil
	}

	return &Enigma{
		engine: engine,
		trans:  trans,
	}
}

type EnigmaUtility interface {
	BindAndValidate(c *gin.Context, payload any) map[string][]string
}

func (v Enigma) BindAndValidate(c *gin.Context, payload any) map[string][]string {
	errs := make(map[string][]string)
	err := c.Bind(payload)
	if err != nil {
		var errJSON *json.UnmarshalTypeError
		if errors.As(err, &errJSON) {
			field := errJSON.Field
			errVal := errJSON.Error()
			return map[string][]string{
				field: {errVal},
			}
		}
		return map[string][]string{
			"error": {err.Error()},
		}
	}
	err = v.engine.Struct(payload)
	if err != nil {
		var errVals validator.ValidationErrors
		if errors.As(err, &errVals) {
			for i, _ := range errVals {
				errs[errVals[i].Field()] = []string{errVals[i].Translate(v.trans)}
			}

			return errs
		}
		return errs
	}

	return errs
}
