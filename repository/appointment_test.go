package repository

import (
	"context"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Nacute/fita-be-test/entities"
	"gitlab.com/Nacute/fita-be-test/utils/test_utils"
	"gorm.io/gorm"
	"log"
	"regexp"
	"testing"
	time2 "time"
)

func TestAppointment_Update(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		ctx         context.Context
		appointment entities.Appointments
		fields      []string
	}

	mockQuery, db, ok := test_utils.NewQueryDBMock()
	if !ok {
		log.Println("error generate mock")
		t.Fail()
	}

	mockQuery.ExpectExec(regexp.QuoteMeta("UPDATE `appoinments` SET `latest_status`=?,`updated_at`=? WHERE `id` = ?")).
		WithArgs("success", time2.Time{}, uint(1)).
		WillReturnResult(sqlmock.NewResult(0, 1))

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entities.Appointments
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				db: db,
			},
			args: args{
				ctx: context.Background(),
				appointment: entities.Appointments{
					ID:           1,
					LatestStatus: "success",
				},
				fields: []string{"latest_status"},
			},
			want: entities.Appointments{
				ID:           1,
				LatestStatus: "success",
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Nil(t, err, i)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := Appointment{
				db: tt.fields.db,
			}
			got, err := repo.Update(tt.args.ctx, tt.args.appointment, tt.args.fields...)
			if !tt.wantErr(t, err, fmt.Sprintf("Update(%v, %v, %v)", tt.args.ctx, tt.args.appointment, tt.args.fields)) {
				return
			}
			assert.Equalf(t, tt.want, got, "Update(%v, %v, %v)", tt.args.ctx, tt.args.appointment, tt.args.fields)
		})
	}
}

func TestAppointment_GetByID(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		ctx context.Context
		id  uint
	}
	mockQuery, db, ok := test_utils.NewQueryDBMock()
	if !ok {
		log.Println("error generate mock")
		t.Fail()
	}

	mockQuery.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `appoinments` WHERE `appoinments`.`id` = ?")).
		WithArgs(uint(1)).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(uint(1)))

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entities.Appointments
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name:   "success",
			fields: fields{db: db},
			args:   args{ctx: context.Background(), id: uint(1)},
			want:   entities.Appointments{ID: uint(1)},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Nil(t, err, i)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := Appointment{
				db: tt.fields.db,
			}
			got, err := repo.GetByID(tt.args.ctx, tt.args.id)
			if !tt.wantErr(t, err, fmt.Sprintf("GetByID(%v, %v)", tt.args.ctx, tt.args.id)) {
				return
			}
			assert.Equalf(t, tt.want, got, "GetByID(%v, %v)", tt.args.ctx, tt.args.id)
		})
	}
}

func TestAppointment_GetByCoachID(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		ctx context.Context
		id  uint
	}

	mockQuery, db, ok := test_utils.NewQueryDBMock()
	if !ok {
		log.Println("error generate mock")
		t.Fail()
	}

	mockQuery.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `appoinments` WHERE coach_id = ?")).
		WithArgs(uint(1)).
		WillReturnRows(sqlmock.NewRows([]string{"coach_id"}).AddRow(uint(1)))

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []entities.Appointments
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name:   "success",
			fields: fields{db: db},
			args:   args{ctx: context.Background(), id: uint(1)},
			want:   []entities.Appointments{{CoachID: uint(1)}},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Nil(t, err, i)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := Appointment{
				db: tt.fields.db,
			}
			got, err := repo.GetByCoachID(tt.args.ctx, tt.args.id)
			if !tt.wantErr(t, err, fmt.Sprintf("GetByCoachID(%v, %v)", tt.args.ctx, tt.args.id)) {
				return
			}
			assert.Equalf(t, tt.want, got, "GetByCoachID(%v, %v)", tt.args.ctx, tt.args.id)
		})
	}
}

func TestAppointment_Store(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		ctx         context.Context
		appointment *entities.Appointments
	}

	mockQuery, db, ok := test_utils.NewQueryDBMock()
	if !ok {
		log.Println("error generate mock")
		t.Fail()
	}

	mockQuery.ExpectExec(regexp.QuoteMeta("INSERT INTO `appoinments`")).
		WillReturnResult(sqlmock.NewResult(1, 1))

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *entities.Appointments
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name:   "success",
			fields: fields{db: db},
			args:   args{ctx: context.Background(), appointment: &entities.Appointments{}},
			want:   &entities.Appointments{ID: uint(1)},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Nil(t, err, i)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := Appointment{
				db: tt.fields.db,
			}
			got, err := repo.Store(tt.args.ctx, tt.args.appointment)
			if !tt.wantErr(t, err, fmt.Sprintf("Store(%v, %v)", tt.args.ctx, tt.args.appointment)) {
				return
			}
			assert.Equalf(t, tt.want, got, "Store(%v, %v)", tt.args.ctx, tt.args.appointment)
		})
	}
}
