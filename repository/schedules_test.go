package repository

import (
	"context"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Nacute/fita-be-test/entities"
	"gitlab.com/Nacute/fita-be-test/utils/test_utils"
	"gorm.io/gorm"
	"log"
	"regexp"
	"testing"
)

func TestScheduleRepo_GetByCoachID(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		ctx context.Context
		id  uint
	}

	mockQuery, db, ok := test_utils.NewQueryDBMock()
	if !ok {
		log.Println("error generate mock")
		t.Fail()
	}

	mockQuery.ExpectQuery(regexp.QuoteMeta("SELECT * FROM `schedules` WHERE coach_id = ?")).
		WithArgs(uint(1)).
		WillReturnRows(sqlmock.NewRows([]string{"coach_id"}).AddRow(uint(1)))

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []entities.Schedules
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				db: db,
			},
			args: args{
				ctx: context.Background(),
				id:  uint(1),
			},
			want: []entities.Schedules{{CoachID: 1}},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Nil(t, err, i)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := ScheduleRepo{
				db: tt.fields.db,
			}
			got, err := repo.GetByCoachID(tt.args.ctx, tt.args.id)
			if !tt.wantErr(t, err, fmt.Sprintf("GetByCoachID(%v, %v)", tt.args.ctx, tt.args.id)) {
				return
			}
			assert.Equalf(t, tt.want, got, "GetByCoachID(%v, %v)", tt.args.ctx, tt.args.id)
		})
	}
}
