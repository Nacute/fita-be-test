package repository

import (
	"context"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Nacute/fita-be-test/utils/test_utils"
	"gorm.io/gorm"
	"log"
	"regexp"
	"testing"
)

func TestUserRepo_IsUserExist(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		ctx context.Context
		id  uint
	}
	mockQuery, db, ok := test_utils.NewQueryDBMock()
	if !ok {
		log.Println("error generate mock")
		t.Fail()
	}

	mockQuery.ExpectQuery(regexp.QuoteMeta("SELECT COUNT(id) > 0 as exist FROM `main_users` WHERE `main_users`.`id` = ?")).
		WithArgs(uint(1)).
		WillReturnRows(sqlmock.NewRows([]string{"exist"}).AddRow(true))

	tests := []struct {
		name      string
		fields    fields
		args      args
		wantExist bool
		wantErr   assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				db: db,
			},
			args: args{
				ctx: context.Background(),
				id:  1,
			},
			wantExist: true,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Nil(t, err, i)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := UserRepo{
				db: tt.fields.db,
			}
			gotExist, err := repo.IsUserExist(tt.args.ctx, tt.args.id)
			if !tt.wantErr(t, err, fmt.Sprintf("IsUserExist(%v, %v)", tt.args.ctx, tt.args.id)) {
				return
			}
			assert.Equalf(t, tt.wantExist, gotExist, "IsUserExist(%v, %v)", tt.args.ctx, tt.args.id)
		})
	}
}
