//go:generate mockery --all --inpackage --case snake

package repository

import (
	"context"
	"gitlab.com/Nacute/fita-be-test/entities"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type Appointment struct {
	db *gorm.DB
}

func NewAppointmentRepo(db *gorm.DB) AppointmentRepoInterface {
	return &Appointment{db: db}
}

type AppointmentRepoInterface interface {
	Store(ctx context.Context, appointment *entities.Appointments) (*entities.Appointments, error)
	GetByCoachID(ctx context.Context, id uint) ([]entities.Appointments, error)
	GetByID(ctx context.Context, id uint) (entities.Appointments, error)
	Update(
		ctx context.Context,
		appointment entities.Appointments,
		fields ...string,
	) (entities.Appointments, error)
}

func (repo Appointment) Update(
	ctx context.Context,
	appointment entities.Appointments,
	fields ...string,
) (entities.Appointments, error) {
	err := repo.db.WithContext(ctx).
		Select(fields).
		Omit(clause.Associations).
		Updates(&appointment).
		Error
	return appointment, err
}

func (repo Appointment) GetByID(ctx context.Context, id uint) (entities.Appointments, error) {
	var appointment = entities.Appointments{}
	err := repo.db.WithContext(ctx).
		Find(&appointment, id).
		Error

	return appointment, err
}

func (repo Appointment) GetByCoachID(ctx context.Context, id uint) ([]entities.Appointments, error) {
	var appointments = make([]entities.Appointments, 0)
	err := repo.db.WithContext(ctx).
		Find(&appointments, "coach_id = ?", id).
		Error

	return appointments, err
}

func (repo Appointment) Store(ctx context.Context, appointment *entities.Appointments) (*entities.Appointments, error) {
	err := repo.db.WithContext(ctx).
		Omit(clause.Associations).
		Create(&appointment).
		Error

	return appointment, err
}
