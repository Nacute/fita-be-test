//go:generate mockery --all --inpackage --case snake

package repository

import (
	"context"
	"gitlab.com/Nacute/fita-be-test/entities"
	"gorm.io/gorm"
)

type UserRepo struct {
	db *gorm.DB
}

func NewUserRepo(db *gorm.DB) UserRepoInterface {
	return &UserRepo{
		db: db,
	}
}

type UserRepoInterface interface {
	IsUserExist(ctx context.Context, id uint) (exist bool, err error)
}

func (repo UserRepo) IsUserExist(ctx context.Context, id uint) (exist bool, err error) {
	err = repo.db.WithContext(ctx).
		Model(&entities.MainUser{}).
		Select("COUNT(id) > 0 as exist").
		Find(&exist, id).
		Error
	return exist, err
}
