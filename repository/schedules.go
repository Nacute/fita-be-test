//go:generate mockery --all --inpackage --case snake

package repository

import (
	"context"
	"gitlab.com/Nacute/fita-be-test/entities"
	"gorm.io/gorm"
)

type ScheduleRepo struct {
	db *gorm.DB
}

func NewScheduleRepo(db *gorm.DB) ScheduleRepoInterface {
	return &ScheduleRepo{db: db}
}

type ScheduleRepoInterface interface {
	GetByCoachID(ctx context.Context, id uint) ([]entities.Schedules, error)
}

func (repo ScheduleRepo) GetByCoachID(ctx context.Context, id uint) ([]entities.Schedules, error) {
	var schedules []entities.Schedules
	err := repo.db.WithContext(ctx).
		Find(&schedules, "coach_id = ?", id).
		Error

	return schedules, err
}
